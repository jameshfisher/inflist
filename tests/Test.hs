{-# LANGUAGE UnicodeSyntax, TypeOperators #-}

module Main where

import System.Exit
import Control.Monad
import Test.QuickCheck
import Test.QuickCheck.Function
import Text.Printf

import Data.InfList
import Data.InfList.Arbitrary

newtype SmallInt = SmallInt Int deriving (Show)
instance Arbitrary SmallInt where
  arbitrary = liftM SmallInt $ choose (0, 1000)

newtype VerySmallInt = VerySmallInt Int deriving (Show)
instance Arbitrary VerySmallInt where
  arbitrary = liftM VerySmallInt $ choose (0, 50)


eq :: Eq a ⇒ Int → InfList a → InfList a → Bool
eq n l l' = (Data.InfList.take n l) == (Data.InfList.take n l')

prop_toList_is_same_list :: InfList Int → SmallInt → Bool
prop_toList_is_same_list l (SmallInt n) = (Data.InfList.take n l) == (Prelude.take n $ Data.InfList.toList l)

prop_head_of_append_is_same_element :: Int → InfList Int → Bool
prop_head_of_append_is_same_element h t = Data.InfList.head (h:::t) == h

prop_tail_of_append_is_same_element :: Int → InfList Int → SmallInt → Bool
prop_tail_of_append_is_same_element h t (SmallInt n) = eq n (Data.InfList.tail (h:::t)) t

prop_map_at_index_is_mapped :: InfList Int → (Fun Int Int) → SmallInt → Bool
prop_map_at_index_is_mapped l (Fun _ f) (SmallInt i) = (Data.InfList.map f l) !!! i == f (l !!! i)

prop_filter_contains_no_unsatisfying_elements :: InfList Int → Bool
prop_filter_contains_no_unsatisfying_elements l = all p $ Data.InfList.take 10 $ Data.InfList.filter p l where p = even

prop_iterate_id_is_repeat :: Int → SmallInt → Bool
prop_iterate_id_is_repeat i (SmallInt n) = eq n (Data.InfList.iterate id i) (Data.InfList.repeat i)

prop_iterate_index_n_is_applied_n_times :: Int → Fun Int Int → SmallInt → Bool
prop_iterate_index_n_is_applied_n_times i (Fun _ f) (SmallInt n) = (Data.InfList.iterate f i !!! n) == (Prelude.foldr (.) id (replicate n f) i)

prop_take_after_prepend_yields_same :: [Int] → InfList Int → Bool
prop_take_after_prepend_yields_same fl il = Data.InfList.take (length fl) (fl +++ il) == fl

prop_drop_after_prepend_yields_same :: [Int] → InfList Int → SmallInt → Bool
prop_drop_after_prepend_yields_same fl il (SmallInt n) = eq n (Data.InfList.drop (length fl) (fl+++il)) il

prop_element_of_list_at_repeated_indices_in_cycle :: [Int] → Int → VerySmallInt → Bool
prop_element_of_list_at_repeated_indices_in_cycle fl i' (VerySmallInt n) = null fl || eq n (Data.InfList.repeat el) (Data.InfList.map (Data.InfList.cycle fl !!!) indices)
  where
    len = length fl
    i = i' `mod` len
    el = fl !! i
    indices = Data.InfList.iterate (+len) i

prop_takeWhile_all_satisfy :: InfList Int → Fun Int Bool → Bool
prop_takeWhile_all_satisfy il (Fun _ p) = all p (Data.InfList.takeWhile p il)

prop_next_after_takeWhile_does_not_satisfy :: InfList Int → Fun Int Bool → Bool
prop_next_after_takeWhile_does_not_satisfy il (Fun _ p) = let fl = (Data.InfList.takeWhile p il) in not $ p $ Data.InfList.head $ Data.InfList.drop (length fl) il

prop_length_of_take_is_length :: InfList Int → SmallInt → Bool
prop_length_of_take_is_length l (SmallInt n) = length (Data.InfList.take n l) == n

prop_length_of_splitAt_is_length :: InfList Int → SmallInt → Bool
prop_length_of_splitAt_is_length l (SmallInt n) = (length $ fst $ Data.InfList.splitAt n l) == n

--span
--break

prop_concatMap_first_elements_are_expanded_first_element :: InfList Int → Fun Int [Int] → Bool
prop_concatMap_first_elements_are_expanded_first_element l (Fun _ f) = (f $ Data.InfList.head l) `Data.InfList.isPrefixOf` (Data.InfList.concatMap f l)

prop_concat_first_elements_are_first_list :: InfList [Int] → Bool
prop_concat_first_elements_are_first_list l = Data.InfList.head l `Data.InfList.isPrefixOf` (Data.InfList.concat l)



tests =
  [ --("`toList` produces the same list",                     q prop_toList_is_same_list)
  --, ("`head` of list is appended element",                  q prop_head_of_append_is_same_element)
  --, ("`tail` of consed list is tail",                       q prop_tail_of_append_is_same_element)
  --, ("`map`ped list at index is function applied at index", q prop_map_at_index_is_mapped)
  --, ("filter contains no unsatisfying elements",            q prop_filter_contains_no_unsatisfying_elements)
  --, ("`iterate id` == `repeat`",                            q prop_iterate_id_is_repeat)
  --, ("`iterate` at index n is applied n times",             q prop_iterate_index_n_is_applied_n_times)
  --, ("take list after prepending it yields same list",      q prop_take_after_prepend_yields_same)
  --, ("dropping list after prepending it yields same list",  q prop_drop_after_prepend_yields_same)
  --, ("element of list occurs at repeated indices in cycled list", q prop_element_of_list_at_repeated_indices_in_cycle)
  --, ("`takeWhile` all satisfy given predicate",             q prop_takeWhile_all_satisfy)
  --, ("next after `takeWhile` does not satisfy predicate",   q prop_next_after_takeWhile_does_not_satisfy)
  --, ("Length of `take` is requested length",                q prop_length_of_take_is_length)
  --, ("length of start of `splitAt` is requested length",    q prop_length_of_splitAt_is_length)
  --, ("first elements after `concatMap` are expanded first element", q prop_concatMap_first_elements_are_expanded_first_element)
  --, ("first elements of `concat`ted list are first list",   q prop_concat_first_elements_are_first_list)
  ] where
    q :: Testable prop => prop -> IO Result
    q = quickCheckResult

main = mapM check tests >>= ret where
  check (s,a) = do
    putStr $ s ++ ":  "
    a
  ret results = if all success results then exitSuccess else exitFailure
    where
      success (Success _ _ _) = True
      success _ = False
