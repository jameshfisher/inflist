{-# LANGUAGE UnicodeSyntax #-}

module Data.InfList.Arbitrary where

import Data.InfList

import Control.Monad
import Test.QuickCheck

instance Arbitrary a ⇒ Arbitrary (InfList a) where
  arbitrary = liftM2 (:::) arbitrary arbitrary
